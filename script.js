// 1. Метод об'єкту - це функція, що викликається для того, щоб щось 
// зробити з даними, що належать цьому об'єкту.
// 2. Будь-які з відомих мені. Всі примітивні + інші об'єкти, масиви, функції.
// 3. Те, що об'єкт є посилальним типом даних означає, що при присвоєнні його значення іншій змінній
// він не копіюється цілком, а зберігається як посилання на себе у назві.

function createNewUser () {
    let newUser = {
        firstName: prompt(`Enter your first name`, ``),
        lastName: prompt(`Enter your last name`, ``),
        getLogin() {
            return `${this.firstName[0].toLowerCase()}${this.lastName.toLowerCase()}`;
        },
    }
    return newUser;
};

let user = createNewUser();
console.log(user.getLogin());